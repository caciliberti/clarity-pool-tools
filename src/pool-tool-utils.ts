import {
  AccountsApi,
  AddressTransactionsListResponse,
  BlocksApi,
  BNSApi,
  Configuration,
  InfoApi,
  SmartContractsApi,
  TransactionsApi,
} from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  ClarityType,
  ClarityValue,
  cvToHex,
  cvToString,
  hexToCV,
  ListCV,
  listCV,
  makeContractCall,
  PrincipalCV,
  ResponseOkCV,
  SomeCV,
  standardPrincipalCV,
  TupleCV,
  tupleCV,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { StacksNetwork } from "@stacks/network";
import { readFileSync, writeFileSync } from "fs";
import {
  handleTransaction,
  mainnet,
  mocknet,
  network,
  STACKS_API_URL,
  timeout,
} from "./deploy";
import { address } from "bitcoinjs-lib";
import { StackingClient } from "@stacks/stacking";
import { poxAddrCVFromBitcoin, poxCVToBtcAddress } from "./utils-pox-addr";

const fetch = require("node-fetch");
import BigNum from "bn.js";
import { GENESIS_CONTRACT_ADDRESS } from "./constants";

export interface DelegationDataCV {
  delegatedTo?: string;
  amountUstx?: BigNum;
  untilBurnHt?: number;
  poxAddress?: string;
}
const delegationDataCV = (cv: ClarityValue): DelegationDataCV | undefined => {
  console.log(JSON.stringify(cv));
  if (cv.type === ClarityType.OptionalSome) {
    const tupleData = ((cv as SomeCV).value as TupleCV).data;
    const untilBurnHtCV = tupleData["until-burn-ht"];
    const poxAddressCV = tupleData["pox-addr"];
    return {
      delegatedTo: cvToString(tupleData["delegated-to"]),
      amountUstx: (tupleData["amount-ustx"] as UIntCV).value,
      untilBurnHt:
        untilBurnHtCV.type === ClarityType.OptionalSome
          ? (untilBurnHtCV.value as UIntCV).value.toNumber()
          : undefined,
      poxAddress:
        poxAddressCV.type === ClarityType.OptionalSome
          ? poxCVToBtcAddress(poxAddressCV.value as TupleCV)
          : undefined,
    };
  } else {
    return undefined;
  }
};

const config = new Configuration({
  basePath: STACKS_API_URL,
  fetchApi: fetch,
});
export const infoApi = new InfoApi(config);
export const contractsApi = new SmartContractsApi(config);
export const transcationsApi = new TransactionsApi(config);
export const accountsApi = new AccountsApi(config);
export const bnsApi = new BNSApi(config);
export const blocksApi = new BlocksApi(config);
const limit = 30;

export const poxContractAddress = mainnet
  ? "SP000000000000000000002Q6VF78"
  : "ST000000000000000000002AMW42H";

export const poolToolContractV0 = mainnet
  ? {
      address: "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
      name: "pool-tool",
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "pool-tool",
    };

export const poolToolContract = mainnet
  ? {
      address: "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
      name: "pool-tool-v1",
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "pool-tool-v1",
    };

export const boomboxes = mainnet
  ? {
      address: "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
      name: "boomboxes-cycle-20-v1",
      rewardCycle: 20,
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "boomboxes-v1",
      rewardCycle: 1,
    };

export const boomboxes2 = mainnet
  ? {
      address: "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
      name: "boomboxes-cycle-20-v2",
      rewardCycle: 20,
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "boomboxes-v1",
      rewardCycle: 1,
    };

export const poolpayoutHintsContract = {
  address: "SP2PABAF9FTAJYNFZH93XENAJ8FVY99RRM50D2JG9",
  name: "friedgerpool-payout-hints",
};

export function getBtcAddress(checksumHex: string, versionHex: string) {
  const checksum = Buffer.from(checksumHex, "hex");
  const version = Buffer.from(versionHex, "hex");
  const btcAddress = address.toBase58Check(
    checksum,
    new BigNum(version).toNumber()
  );
  return btcAddress;
}

export function stackersToCycle(
  stackers: {
    rewardCycle: number;
    amount: number;
    stacker: string;
  }[],
  cycle: number
) {
  const members = new Array(...new Set(stackers)).filter(
    (s: any) =>
      s.rewardCycle + s.lockingPeriod > cycle && s.rewardCycle <= cycle
  );
  return {
    cycle,
    members,
    count: members.length,
    total: members.reduce((sum, member) => sum + member.amount, 0),
  };
}
export function getPayoutHints() {
  const txsFilePath = `acc-txs-${network.chainId}-${poolpayoutHintsContract.address}.${poolpayoutHintsContract.name}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());
  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  const hints: any = {};
  let lastBlockHeight = 0;
  for (const tx of txs) {
    lastBlockHeight = tx.block_height;
    if (
      !hints[tx.sender_address] ||
      hints[tx.sender_address].blockHeight < tx.block_height
    ) {
      switch (tx.contract_call.function_name) {
        case "set-payout-recipient":
          hints[tx.sender_address] = {
            recipient: tx.contract_call.function_args[0].repr,
            blockHeight: tx.block_height,
          };
          break;
        case "delete-payout-recipient":
          (hints as any)[tx.sender_address] = {
            recipient: undefined,
            blockHeight: tx.block_height,
          };
          break;
        default:
      }
    }
  }
  return hints;
}

export function getStackersFromPoolTool(
  contract: string,
  network: StacksNetwork,
  excludeAdmin?: string,
  adminOnly?: string
) {
  return getStackersFromContract(
    contract,
    network,
    (tx) => {
      switch (tx.contract_call.function_name) {
        case "delegate-stack-stx":
        case "delegate-stack-stx-and-commit":
          const lockingPeriod = (
            hexToCV(tx.contract_call.function_args[3].hex) as any
          ).value.toNumber();
          const rewardCycle = (
            hexToCV(tx.contract_call.function_args[4].hex) as any
          ).value.toNumber();
          try {
            const events = (
              (hexToCV(tx.tx_result.hex) as ResponseOkCV).value as TupleCV
            ).data["stack-result"] as ListCV;
            return events.list.map((e: any) => [
              e.success,
              e.value.data["lock-amount"].value.toNumber(),
              cvToString(e.value.data["stacker"]),
              e.value.data["unlock-burn-height"].value.toNumber(),
              rewardCycle,
              lockingPeriod,
            ]);
          } catch (e) {
            console.log(tx.tx_id, tx.tx_result.repr, e);
            return [];
          }

        default:
          return [];
      }
    },
    excludeAdmin,
    adminOnly
  );
}

export function getStackersFromPox(
  network: StacksNetwork,
  poolAdmins: { stacks: string }[]
) {
  return getStackersFromContract(
    GENESIS_CONTRACT_ADDRESS + ".pox",
    network,
    (tx: any) => {
      switch (tx.contract_call.function_name) {
        case "delegate-stack-stx":
          try {
            const poxDetails: any = (hexToCV(tx.tx_result.hex) as ResponseOkCV)
              .value as TupleCV;

            const unlockBurnHeight =
              poxDetails.data["unlock-burn-height"].value.toNumber();
            const untilCycle = (unlockBurnHeight - firstBurnChainBlock) / 2100;

            const rewardCycle =
              tx.block_height > 35_000
                ? 20
                : tx.block_height > 33_500
                ? 19
                : tx.block_height > 31_500
                ? 18
                : tx.block_height > 30_000
                ? 17
                : tx.block_height > 28_000
                ? 16
                : 0;
            if (rewardCycle <= 0) {
              throw new Error("invalid reward cycle");
            }
            const lockingPeriod = untilCycle - rewardCycle;
            return [
              [
                true,
                poxDetails.data["lock-amount"].value.toNumber(),
                cvToString(poxDetails.data["stacker"]),
                unlockBurnHeight,
                rewardCycle,
                lockingPeriod,
                tx.block_height,
              ],
            ]; // array of 1 stacker
          } catch (e) {
            console.log(tx.tx_id, tx.tx_result.repr, e);
            return []; // array of 0 stacker
          }

        default:
          return []; // array of 0 stacker
      }
    },
    undefined,
    undefined,
    poolAdmins
  );
}

export function getStackersFromBoombox(
  contract: string,
  rewardCycle: number,
  network: StacksNetwork,
  excludeAdmin?: string,
  adminOnly?: string
) {
  return getStackersFromContract(
    contract,
    network,
    (tx: any) => {
      switch (tx.contract_call.function_name) {
        case "delegate-stx":
          const lockingPeriod = 1;
          try {
            const poxDetails: any = (
              (hexToCV(tx.tx_result.hex) as ResponseOkCV).value as TupleCV
            ).data["pox"];
            return [
              [
                true,
                poxDetails.data["lock-amount"].value.toNumber(),
                cvToString(poxDetails.data["stacker"]),
                poxDetails.data["unlock-burn-height"].value.toNumber(),
                rewardCycle,
                lockingPeriod,
              ],
            ]; // array of 1 stacker
          } catch (e) {
            console.log(tx.tx_id, tx.tx_result.repr, e);
            return []; // array of 0 stacker
          }

        default:
          return []; // array of 0 stacker
      }
    },
    excludeAdmin,
    adminOnly
  );
}

export function getStackersFromContract(
  contract: string,
  network: StacksNetwork,
  txToStackers: (tx: any) => any[],
  excludeAdmin?: string,
  adminOnly?: string,
  poolAdmins?: { stacks: string }[]
) {
  const txsFilePath = `acc-txs-${network.chainId}-${contract}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());
  txs = txs.filter(
    (t: any) =>
      t.tx_status === "success" &&
      t.tx_type === "contract_call" &&
      (!excludeAdmin || t.sender_address !== excludeAdmin) &&
      (!adminOnly || t.sender_address === excludeAdmin) &&
      (!poolAdmins ||
        poolAdmins.findIndex((p) => t.sender_address === p.stacks) >= 0)
  );
  let stackers: any[] = [];
  for (const tx of txs) {
    let stackersInTx: any[] = txToStackers(tx);
    stackers = stackers.concat(
      stackersInTx.map((details) => {
        return {
          timestamp: tx.burn_block_time_iso,
          blockheight: tx.block_height,
          txid: tx.tx_id,
          success: details[0],
          amount: details[1],
          stacker: details[2],
          unlock: details[3],
          rewardCycle: details[4],
          lockingPeriod: details[5],
          blockHeight: details[6],
        };
      })
    );
  }
  return stackers;
}

export function logStackersCSV(stackers: any) {
  for (let s of stackers) {
    console.log(
      [
        s.timstamp,
        s.block_height,
        "",
        s.stacker,
        s.amount / 1000000,
        s.unlock,
        s.lockingPeriod,
        s.rewardCycle,
      ].join(", ")
    );
  }
}

export function logAccountTxsCSV(address: string, network: StacksNetwork) {
  const lines = [];
  const txsFilePath = `acc-txs-${network.chainId}-${address}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  lines.push(
    Array.of("time", "block", "fee-rate", "function", "sender").join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      default:
        details = [tx.tx_result.repr].concat(
          tx.contract_call.function_args
            ? tx.contract_call.function_args.map((a: any) => a.repr)
            : []
        );
    }
    lines.push(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.fee_rate,
        tx.contract_call.contract_id,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
  writeFileSync(txsFilePath + ".csv", lines.join("\n"));
}

export function logPoolToolContractCSV(network: StacksNetwork) {
  const txsFilePath = `acc-txs-${network.chainId}-${poolToolContract.address}.${poolToolContract.name}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(
    Array.of(
      "time",
      "block",
      "function",
      "sender",
      "amount",
      "pool-admin",
      "block-height",
      "locking-cycles",
      "pox-address"
    ).join(",")
  );

  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "delegate-stack-stx-and-commit":
        details = [
          "",
          "",
          tx.contract_call.function_args[2].repr, // start-burn-ht
          tx.contract_call.function_args[3].repr, // lock-period
          tx.contract_call.function_args[1].repr, // pox-address
          tx.contract_call.function_args[4].repr, // reward-cycle
        ];
        break;
      default:
        details = [];
    }
    console.log(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
}
export function logPoxContractCSV(network: StacksNetwork) {
  const lines = [];
  const txsFilePath = `acc-txs-${network.chainId}-${poxContractAddress}.pox.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  lines.push(
    Array.of(
      "time",
      "block",
      "fee-rate",
      "function",
      "sender",
      "amount",
      "pool-admin",
      "block-height",
      "locking-cycles",
      "pox-address"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "stack-stx":
        details = [
          tx.contract_call.function_args[0].repr.substr(1) / 1000000,
          "",
          tx.contract_call.function_args[2].repr.substr(1),
          tx.contract_call.function_args[3].repr.substr(1),
          tx.contract_call.function_args[1].repr,
        ];
        break;
      case "delegate-stx":
        details = [
          tx.contract_call.function_args[0].repr.substr(1) / 1000000,
          tx.contract_call.function_args[1].repr,
          "",
          tx.contract_call.function_args[3].repr,
          tx.contract_call.function_args[2].repr,
        ];
        break;
      case "delegate-stack-stx":
        details = [
          tx.contract_call.function_args[1].repr.substr(1) / 1000000,
          tx.contract_call.function_args[0].repr,
          tx.contract_call.function_args[3].repr.substr(1),
          tx.contract_call.function_args[4].repr.substr(1),
          tx.contract_call.function_args[2].repr,
        ];
        break;
      case "allow-contract-caller":
        details = ["", tx.contract_call.function_args[0].repr];
        break;
      case "disallow-contract-caller":
        details = ["", tx.contract_call.function_args[0].repr];
        break;
      case "revoke-delegate-stx":
      default:
        details = [];
    }
    lines.push(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.fee_rate,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
  const suffix = new Date().toISOString();
  writeFileSync(txsFilePath + suffix + ".csv", lines.join("\n"));
}

export function logBoomboxesContractCSV(network: StacksNetwork) {
  const txsFilePath = `acc-txs-${network.chainId}-${boomboxes.address}.${boomboxes.name}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(
    Array.of(
      "time",
      "block",
      "function",
      "sender",
      "amount",
      "pool-admin",
      "block-height",
      "locking-cycles",
      "pox-address"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "delegate-stx":
        const boomboxId = (hexToCV(tx.tx_result.hex) as any).value.data[
          "id"
        ].value.toNumber();
        details = [
          tx.contract_call.function_args[0].repr.substr(1) / 1000000,
          tx.contract_call.function_args[1].repr,
          "",
          tx.contract_call.function_args[3].repr,
          tx.contract_call.function_args[2].repr,
          boomboxId,
        ];
        break;
      case "transfer":
        details = [
          "",
          tx.contract_call.function_args[2].repr,
          "",
          "",
          tx.contract_call.function_args[0].repr.substr(1),
        ];
        break;
      default:
        details = [];
    }
    console.log(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
}
export const firstBurnChainBlock = 666050;
export const cycleLength = 2100;

export function burnHeightToCycle(burnHeight?: number): number | undefined {
  return burnHeight
    ? Math.ceil((burnHeight - firstBurnChainBlock) / cycleLength)
    : undefined;
}

export async function getFilteredDelegationStates(
  admin: { stacks: string },
  minUntilBurnHt: number | undefined = undefined,
  onlyUnstackedMembers: boolean,
  onlyExactUntilBurnHt: boolean
) {
  const allStates = getDelegationStates(admin.stacks);
  let delegationStates = allStates.find(
    (poolData: any) => poolData.delegatee === admin.stacks
  )?.stackerStates;

  delegationStates = delegationStates?.filter(
    // only log good members
    (s: any) =>
      s.stacker !== admin.stacks && // ignore the admin
      s.data &&
      s.data.delegatedTo === admin.stacks && // ignore members stacking to other pools
      (!onlyUnstackedMembers || !s.status.stacked) && // ignore members that are already stacked
      (onlyExactUntilBurnHt ||
        !minUntilBurnHt ||
        !s.data.untilBurnHt ||
        s.data.untilBurnHt > minUntilBurnHt) && // ignore members with expiring delegation
      (!onlyExactUntilBurnHt || s.data.untilBurnHt === minUntilBurnHt)
  );
  return delegationStates;
}

export async function logDelegationStatesCSV(
  admin: { stacks: string },
  onlyUnstackedMembers: boolean = true
) {
  const delegationStates = await getFilteredDelegationStates(
    admin,
    undefined,
    onlyUnstackedMembers,
    false
  );
  if (delegationStates) {
    delegationStates.forEach((s: any) => {
      console.log(
        `${s.stacker}, ${s.status.stacked}, ${parseInt(
          s.data.amountUstx,
          16
        ).toString()}, ${admin.stacks}, ${
          s.data.untilBurnHt
        }, ${burnHeightToCycle(s.data.untilBurnHt)}`
      );
    });
  }
}

export async function downloadBoomboxesTxs(network: StacksNetwork) {
  return downloadAccountTxs(`${boomboxes.address}.${boomboxes.name}`, network);
}

export async function downloadPoolPayoutHints(network: StacksNetwork) {
  return downloadAccountTxs(
    `${poolpayoutHintsContract.address}.${poolpayoutHintsContract.name}`,
    network
  );
}

export async function downloadPoolToolTxs(network: StacksNetwork) {
  return downloadAccountTxs(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
}

export async function downloadPoolToolV0Txs(network: StacksNetwork) {
  return downloadAccountTxs(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
}

export async function downloadPoxTxs(network: StacksNetwork) {
  return downloadAccountTxs(`${poxContractAddress}.pox`, network);
}

export async function downloadXverseTxs(network: StacksNetwork) {
  return downloadAccountTxs(
    "SPXVRSEH2BKSXAEJ00F1BY562P45D5ERPSKR4Q33.xverse-pool-v1",
    network
  );
}

export async function downloadFPWRTxs(network: StacksNetwork) {
  return downloadAccountTxs(
    "SP1JSH2FPE8BWNTP228YZ1AZZ0HE0064PS6RXRAY4.fpwr-v03",
    network
  );
}
export async function downloadAccountTxs(
  principal: string,
  network: StacksNetwork
) {
  const txsFilePath = `acc-txs-${network.chainId}-${principal}.json`;
  let txs: any[] = [];
  try {
    txs = JSON.parse(readFileSync(txsFilePath).toString());
  } catch (e) {
    console.log(e);
    txs = [];
  }
  const lastBlock = txs.reduce(
    (seenBlock: number, tx) => Math.max(seenBlock, tx.block_height),
    0
  );
  console.log({ lastBlock });
  let results: AddressTransactionsListResponse;
  let offset = 0;
  let downloadedTxs: any[] = [];
  let newTxs: any[] = [];
  do {
    results = await accountsApi.getAccountTransactions({
      principal,
      offset,
      limit,
    });
    newTxs = results.results.filter(
      (newTx: any) => txs.findIndex((t) => t.tx_id === newTx.tx_id) < 0
    );
    downloadedTxs = downloadedTxs.concat(newTxs);
    offset = offset + results.results.length;
  } while (results.results.length === limit && newTxs.length > 0);
  console.log(txs.length, downloadedTxs.length);
  txs = downloadedTxs.concat(txs);
  writeFileSync(txsFilePath, JSON.stringify(txs));
  return txs;
}

export async function downloadMempoolTxs(
  suffix: string,
  network: StacksNetwork
) {
  const txsFilePath = `mempool-txs-${network.chainId}-${suffix}.json`;
  let txs: any[] = [];
  try {
    txs = JSON.parse(readFileSync(txsFilePath).toString());
  } catch (e) {
    console.log(e);
    txs = [];
  }
  const lastBlock = txs.reduce(
    (seenBlock: number, tx) => Math.max(seenBlock, tx.block_height),
    0
  );
  console.log({ lastBlock });
  let results: AddressTransactionsListResponse;
  let offset = 0;
  let limit = 100;
  let downloadedTxs: any[] = [];
  let newTxs: any[] = [];
  do {
    results = await transcationsApi.getMempoolTransactionList({
      offset,
      limit,
    });
    newTxs = results.results.filter(
      (newTx: any) => txs.findIndex((t) => t.tx_id === newTx.tx_id) < 0
    );
    downloadedTxs = downloadedTxs.concat(newTxs);
    offset = offset + results.results.length;
  } while (results.results.length === limit && newTxs.length > 0);
  console.log(txs.length, downloadedTxs.length);
  txs = downloadedTxs.concat(txs);
  writeFileSync(txsFilePath, JSON.stringify(txs));
  return txs;
}

export function txAsString(t: any) {
  return `${t.tx_id}, ${t.nonce}, ${t.fee_rate}, ${t.tx_type}, ${
    t.receipt_time
  }, ${t.sender_address}, ${
    t.contract_call ? t.contract_call.contract_id : ""
  }, ${t.contract_call ? t.contract_call.function_name : ""}`;
}

export function logMempoolTxsCSV(suffix: string, network: StacksNetwork) {
  const txsFilePath = `mempool-txs-${network.chainId}-${suffix}.json`;
  let txs: any[] = [];
  try {
    txs = JSON.parse(readFileSync(txsFilePath).toString());
  } catch (e) {
    console.log(e);
    txs = [];
  }
  writeFileSync(
    txsFilePath + ".csv",
    "tx_id, nonce, fee_rate, tx_type, receipt_time, sender_address, contract_id, function_name\n" +
      txs.map((t) => txAsString(t)).join("\n")
  );
}

export function logFTTxsAccountCSV(account: string) {
  const txsFilePath = `acc-txs-${network.chainId}-${account}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );

  const output = [];

  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "transfer":
        details = [
          (
            hexToCV(tx.contract_call.function_args[0].hex) as UIntCV
          ).value.toNumber(),
          tx.contract_call.function_args[2].repr,
        ];
        break;
      case "mint":
        if (tx.contract_call.function_args.length > 0) {
          try {
            details = [
              (
                hexToCV(tx.contract_call.function_args[0].hex) as UIntCV
              ).value.toNumber(),
            ];
          } catch (e) {
            details = [];
          }
        } else {
          details = [];
        }
        break;
      default:
        details = [];
    }
    output.push(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.contract_call.contract_id,
        tx.fee_rate,
        ...details
      ).join(",")
    );
  }
  writeFileSync(
    txsFilePath + ".csv",
    "time, block, function, contract_id, fee, amount\n" + output.join("\n")
  );
}

export function getDelegationStates(
  poolAdmin = "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60"
) {
  const statesFilePath = `delegation-states-${network.chainId}-${poolAdmin}.json`;
  return JSON.parse(readFileSync(statesFilePath).toString()) as {
    delegatee: string;
    stackerStates: {
      stacker: string;
      data: DelegationDataCV;
      status: any;
    }[];
  }[];
}

const rewardsBtcAddress = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";
const rewardPoxAddrCV = poxAddrCVFromBitcoin(rewardsBtcAddress);
const cycleId = 17;
const startBlock = firstBurnChainBlock + cycleId * cycleLength - 100; // start block of next cycle
const startBurnHeight = startBlock - 150;

export async function writeDelegationStates(
  txs: any[],
  poolAdmin = "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
  lockedStackers: { stacker: string; unlock: number }[],
  allStates: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[],
  fromBlock?: number
) {
  const statesFilePath = `delegation-states-${network.chainId}-${poolAdmin}.json`;
  const summaryFilePath = `summary-${network.chainId}-${poolAdmin}.json`;

  const delegateStxTxs = txs.filter((tx: any) => {
    if (
      tx.tx_type === "contract_call" &&
      tx.contract_call.function_name === "delegate-stx" &&
      tx.tx_status === "success"
    ) {
      const delegatee = tx.contract_call.function_args[1].repr;
      const stacker = tx.sender_address;
      return (
        (!poolAdmin || delegatee === poolAdmin) &&
        (!fromBlock || tx.block_height >= fromBlock) &&
        lockedStackers.findIndex((ls) => ls.stacker === stacker) < 0
      );
    } else {
      return false;
    }
  });
  console.log({ stackersToCheck: delegateStxTxs.length, poolAdmin });

  const pools = delegateStxTxs.reduce((poolList: any, delegateStxTx: any) => {
    const delegatee = delegateStxTx.contract_call.function_args[1].repr;
    const stacker = delegateStxTx.sender_address;
    const amount = parseInt(
      delegateStxTx.contract_call.function_args[0].repr.substr(1)
    );
    if (poolList[delegatee]) {
      poolList[delegatee].total += amount;
      poolList[delegatee].count += 1;
      poolList[delegatee].stackers[stacker] = { amount };
    } else {
      const stackers: any = {};
      stackers[stacker] = { amount };
      poolList[delegatee] = {
        total: amount,
        count: 1,
        stackers,
      };
    }
    return poolList;
  }, {});

  const delegationStates = await Promise.all(
    Object.keys(pools).map(async (delegatee) => {
      const stackerStates: {
        stacker: string;
        data: DelegationDataCV | undefined;
        status: any;
      }[] = [];

      for (let stacker of Object.keys(pools[delegatee].stackers)) {
        console.log(stacker, poxContractAddress, delegatee);
        const state = allStates.find((s) => s.stacker === stacker);
        if (state) {
          stackerStates.push(state);
        } else {
          const key = cvToHex(
            tupleCV({ stacker: standardPrincipalCV(stacker) })
          );
          const stackingClient = new StackingClient(stacker, network);
          try {
            await updateStackerState(
              stackingClient,
              key,
              stacker,
              stackerStates,
              allStates
            );
          } catch (e) {
            console.log(e);
            await timeout(5000);
            try {
              await updateStackerState(
                stackingClient,
                key,
                stacker,
                stackerStates,
                allStates
              );
            } catch (e) {
              console.log(e);
            }
          }
        }
      }

      return { delegatee, stackerStates };
    })
  );

  const isStillPoolMember = (
    delegatee: string,
    stackerState: { data: DelegationDataCV | undefined }
  ) => stackerState.data && delegatee === stackerState.data.delegatedTo;

  const summarizedStates = delegationStates.map((poolData) => {
    console.log("reducing", poolData.stackerStates.length, poolData.delegatee);
    return poolData.stackerStates.reduce(
      (result, stackerState) => {
        console.log(stackerState);
        const isMember = isStillPoolMember(poolData.delegatee, stackerState);
        if (isMember) {
          /*
          // stack user's STXs
          const stackerCV = standardPrincipalCV(stackerState.stacker);
          const amountUstxCV = uintCV(stackerState.data.amountUstx.toNumber());
          
          const poolAdminLabel = Object.keys(keys).find(
            (k) => keys[k].stacks === poolData.delegatee
          );
          const lockingPeriod = untilBurnHtToLockingPeriod(stackerState.data.untilBurnHt);
          const nonce = poolNonces[poolAdminLabel].nonce 
          if (poolAdminLabel && !stackerState.data.poxAddress || stackerState.data.poxAddress === rewardsBtcAddress) {
            const tx = await makePoxDelegateStackStxCall({
              stackerCV,
              amountUstxCV,
              rewardPoxAddrCV,
              startBurnHeight,
              lockingPeriod,
              poolAdmin: keys[poolAdminLabel],
              nonce,
            });
          }
          */
        }

        return {
          delegatee: poolData.delegatee,
          totals: result.totals.add(
            isMember
              ? (stackerState.data?.amountUstx as BigNum) || new BigNum("0")
              : new BigNum("0")
          ),
          count: result.count + (isMember ? 1 : 0),
          last: stackerState.stacker,
        };
      },
      {
        delegatee: "",
        totals: new BigNum("0"),
        count: 0,
        last: "",
      }
    );
  });
  writeFileSync(summaryFilePath, JSON.stringify(summarizedStates));
  writeFileSync(statesFilePath, JSON.stringify(delegationStates));

  return delegationStates;
}

async function updateStackerState(
  stackingClient: StackingClient,
  key: string,
  stacker: string,
  stackerStates: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[],
  allStates: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[]
) {
  const status = await stackingClient.getStatus();
  const balance = await stackingClient.getAccountBalance();
  console.log(status);
  const state = await contractsApi.getContractDataMapEntry({
    contractAddress: poxContractAddress,
    contractName: "pox",
    mapName: "delegation-state",
    key,
    proof: 0,
  });
  const data = delegationDataCV(hexToCV(state.data));
  const newState = {
    stacker,
    data,
    balance,
    status,
  };
  stackerStates.push(newState);
  allStates.push(newState);
  console.log(stackerStates.length);
}

export async function stackDelegatedStxsInBatches(
  indices: number[] | undefined,
  length: number,
  rewardPoxAddrCV: ClarityValue,
  startBurnHeight: number,
  lockingPeriod: number,
  minUntilBurnHt: number | undefined,
  rewardCycleId: number,
  onlyExactUntilBurnHt: boolean,
  poolAdmin: { stacks: string; private: string },
  nextAccountNonce: number
) {
  // get delegation states from file and filter
  const delegationStates =
    (await getFilteredDelegationStates(
      poolAdmin,
      minUntilBurnHt,
      true,
      onlyExactUntilBurnHt
    )) || [];

  let countTxsSent = 0;

  const numberOfIndices = Math.ceil(delegationStates.length / length);
  if (!indices) {
    indices = [...Array(numberOfIndices).keys()].map((i) => i * length);
  }
  if (indices.length) {
    console.log(
      poolAdmin.stacks,
      delegationStates.length,
      JSON.stringify(indices)
    );
  }
  for (let i = 0; i < indices.length; i++) {
    let index = indices[i];
    // get slice of member for batched stacking
    // as defined by indices and length
    let members = delegationStates
      .slice(index, index + length)
      .map((state: any) =>
        tupleCV({
          stacker: standardPrincipalCV(state.stacker),
          "amount-ustx": uintCV(
            state.stacker === "SP132XA5EFZG9FP31PAGWEE6AP0E6PJMJ4DJARXC5"
              ? new BigNum(1_000_000_000_000).toString(10)
              : new BigNum(state.data.amountUstx, "hex").toString(10)
          ),
        })
      );
    const useBatches = false;
    if (useBatches) {
      let nonce = nextAccountNonce + countTxsSent;
      const membersList = listCV(members);

      // log progress
      console.log(
        index,
        members.length, // count
        cvToString(members[0]), // first in batch,
        nonce,
        lockingPeriod
      );

      // make contract call
      const tx = await makeContractCall({
        contractAddress: poolToolContract.address,
        contractName: poolToolContract.name,
        functionName: "delegate-stack-stx-and-commit",
        functionArgs: [
          membersList,
          rewardPoxAddrCV,
          uintCV(startBurnHeight),
          uintCV(lockingPeriod),
          uintCV(rewardCycleId),
        ],
        senderKey: poolAdmin.private,
        nonce: new BigNum(nonce),
        network: network,
        fee: new BigNum(10000),
        anchorMode: AnchorMode.Any,
      });

      if (nextAccountNonce + i >= 189) {
        await handleTransaction(tx);
        countTxsSent += 1;
        // wait for the stacks node to update the nonce
        if (mocknet) {
          await timeout(5000);
        } else {
          //await timeout(120000);
        }
      } else {
        countTxsSent += 1;
      }
    } else {
      for (let member of members) {
        let nonce = nextAccountNonce + countTxsSent;
        const stackerCV = member.data["stacker"] as PrincipalCV;
        // find min amount
        const stxBalanceResponse = await accountsApi.getAccountStxBalance({
          principal: cvToString(stackerCV),
        });
        const amountUstxCV = {
          type: ClarityType.UInt,
          value: BigNum.min(
            (member.data["amount-ustx"] as UIntCV).value,
            new BigNum((stxBalanceResponse as any).balance)
          ),
        } as UIntCV;

        const tx = await makePoxDelegateStackStxCall({
          stackerCV,
          amountUstxCV,
          rewardPoxAddrCV,
          startBurnHeight,
          lockingPeriod,
          poolAdmin,
          nonce,
        });
        console.log(
          `${cvToString(stackerCV)}, ${cvToString(
            amountUstxCV
          )}, ${nonce}, ${lockingPeriod}, ${tx.txid()}`
        );
        if (nonce >= 0 && amountUstxCV.value.toNumber() > 0) {
          let transaction: { tx_status: string } | undefined = undefined;
          const accountInfo = await accountsApi.getAccountInfo({
            principal: poolAdmin.stacks,
          });
          if (nonce >= accountInfo.nonce) {
            try {
              transaction = (await transcationsApi.getTransactionById({
                txId: tx.txid(),
                unanchored: true,
              })) as { tx_status: string };
            } catch {}
            if (!transaction || transaction.tx_status === "pending") {
              console.log({ txid: tx.txid() });
              try {
                await handleTransaction(tx);
              } catch (e) {
                if (e.toString().includes("ConflictingNonceInMempool")) {
                  // ignore
                  console.log("not replacing ", tx.txid());
                } else {
                  throw e;
                }
              }
              // wait for the stacks node to update the nonce
              if (mocknet) {
                await timeout(5000);
              } else {
                await timeout(30000);
              }
            }
          }
          countTxsSent += 1;
        }
      }
    }
  }

  return { countTxsSent };
}

async function makePoxDelegateStackStxCall({
  stackerCV,
  amountUstxCV,
  rewardPoxAddrCV,
  startBurnHeight,
  lockingPeriod,
  poolAdmin,
  nonce,
}: {
  stackerCV: PrincipalCV;
  amountUstxCV: UIntCV;
  rewardPoxAddrCV: ClarityValue;
  startBurnHeight: number;
  lockingPeriod: number;
  poolAdmin: { private: string };
  nonce: number;
}) {
  return await makeContractCall({
    contractAddress: poxContractAddress,
    contractName: "pox",
    functionName: "delegate-stack-stx",
    functionArgs: [
      stackerCV,
      amountUstxCV,
      rewardPoxAddrCV,
      uintCV(startBurnHeight),
      uintCV(lockingPeriod),
    ],
    senderKey: poolAdmin.private,
    nonce: new BigNum(nonce),
    network: network,
    fee: new BigNum(1200),
    anchorMode: AnchorMode.Any,
  });
}
