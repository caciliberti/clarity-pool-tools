import chai, { expect } from "chai";
import chaiString from "chai-string";
import * as c32 from "c32check";
import {
  poxAddrCV,
  poxCVToBtcAddress,
} from "../../src/utils-pox-addr";
import {
  bufferCV,  
  cvToJSON,
  cvToString,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { address } from "bitcoinjs-lib";

chai.use(chaiString);

describe("pool tool utils", () => {
  it("should create the correct pox address from stx address", async () => {
    const cv = poxAddrCV("SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60");
    expect(cvToString(cv)).equal(
      "(tuple (hashbytes 0x661506d48705f932af21abcff23046b216886e84) (version 0x00))"
    );
  });

  it("should create the correct pox address from multisig btc address", async () => {
    const btcAddress = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";
    const cv = poxAddrCV(c32.b58ToC32(btcAddress));
    expect(cvToString(cv)).equal(
      "(tuple (hashbytes 0x13effebe0ea4bb45e35694f5a15bb5b96e851afb) (version 0x01))"
    );
    expect(poxCVToBtcAddress(cv)).equal(btcAddress);
  });

  it("should generate the correct btc address using btc version multisig btc address", async () => {
    const btcAddress = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";
    const hash = Buffer.from("13effebe0ea4bb45e35694f5a15bb5b96e851afb", "hex");
    expect(address.toBase58Check(hash, 5)).equal(btcAddress);
    expect(
      poxCVToBtcAddress(
        tupleCV({
          hashbytes: bufferCV(hash),
          version: bufferCV(Buffer.from([1])),
        })
      )
    ).equal(btcAddress);
  });

  it("should convert uintCV to hex", async () => {
    const value = cvToJSON(uintCV("1000000000000000"));
    expect(JSON.stringify(value)).equal('{"type":"uint","value":1000000000000000}');
  });
});
