import { AccountsApi } from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  callReadOnlyFunction,
  cvToString,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  sponsorTransaction,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import { accountsApi, boomboxes } from "../src/pool-tool-utils";
import { keys } from "./config";

const BN = require("bn.js");

const { poolAdmin, fpoo1Payout } = keys;

const dryrun = true;
const payStacksTipCV = uintCV(35_000); // stacks height for payout
const payoutFee = new BN(10_000);
const rewardCycle = 20;

async function payoutSet(
  nftIds: number[],
  totalPayoutOfSet: number,
  nonce: number,
  sponsorNonce: number
) {
  const rewardUstxCV = uintCV(totalPayoutOfSet);
  const tx = await makeContractCall({
    contractAddress: boomboxes.address,
    contractName: boomboxes.name,
    functionName: "payout",
    functionArgs: [
      rewardUstxCV,
      listCV(
        nftIds.map((id) => {
          return uintCV(id);
        })
      ),
      payStacksTipCV,
    ],    
    senderKey: fpoo1Payout.private,
    sponsored: true,
    fee: new BN(payoutFee),
    nonce: nonce ? new BN(nonce) : undefined,
    network,
    postConditions: [
      makeStandardSTXPostCondition(
        fpoo1Payout.stacks,
        FungibleConditionCode.LessEqual,
        new BN(totalPayoutOfSet + 1_000_000)
      ),
    ],
    anchorMode: AnchorMode.Any,
    
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(payoutFee),
      sponsorPrivateKey: poolAdmin.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result });
  }
}

(async () => {
  const numberOfBoomboxes = 309;
  // copied from 4-payout.ts
  const rewardsBTC = 2.11523049;
  const exchangeRate = 3535;
  const rewardsBTC2 = 0;
  const exchangeRate2 = 2534;

  const totalRewards =
    (rewardsBTC * 100000000 * 1000000) / exchangeRate +
    (rewardsBTC2 * 100000000 * 1000000) / exchangeRate2;

  const nftIds = [...Array(numberOfBoomboxes).keys()].map((i) => i + 1);
  console.log(nftIds);
  /*
  select sum(sle.locked_amount) from stx_lock_events sle 
join nft_events ne on sle.tx_id = ne.tx_id  
join txs t on t.tx_id = ne.tx_id 
where t.canonical = true and t.microblock_canonical = true and
sle.canonical = true and sle.microblock_canonical = true and 
ne.canonical = true and ne.microblock_canonical = true and
('x' || right(ne.value::text, 6))::bit(24)::int  > 200 and
t.contract_call_contract_id  = 'SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW.boomboxes-cycle-18'

  118160100428 // <= 100
  94140000214 // > 100  && <= 200
  251997520427 // > 200
  */
  const totalStacked = 15158243109919; // from members-x.json
  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  nonce = 14
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let sponsorNonce = sponsorAccountInfo.nonce;
  sponsorNonce = 1950
  const boomboxStacked1 = 118160100428; // from analysis-friedger-pool-boomboxes (file members-x-bb.json)
  const boomboxRewards1 = (boomboxStacked1 * totalRewards) / totalStacked;
  const boomboxStacked2 = 94140000214; // from analysis-friedger-pool-boomboxes (file members-x-bb.json)
  const boomboxRewards2 = (boomboxStacked2 * totalRewards) / totalStacked;
  const boomboxStacked3 = 251997520427; // from analysis-friedger-pool-boomboxes (file members-x-bb.json)
  const boomboxRewards3 = (boomboxStacked3 * totalRewards) / totalStacked;
  console.log({
    numberOfStackers: nftIds.length,
    totalStacked,
    totalRewards,
    boomboxStacked1,
    boomboxRewards1,
    boomboxStacked2,
    boomboxRewards2,
    boomboxStacked3,
    boomboxRewards3,
    boomboxTotal: boomboxStacked1 + boomboxStacked2 + boomboxStacked3,
    boomboxRewardsTotal: boomboxRewards1 + boomboxRewards2 + boomboxRewards3,
  });

  const set1 = nftIds.filter((i) => i <= 100).slice(0, 750);
  await payoutSet(set1, boomboxRewards1, nonce + 3, sponsorNonce + 3);  
  const set2 = nftIds.filter((i) => i > 100 && i <= 200).slice(0, 750);
  await payoutSet(set2, boomboxRewards2, nonce + 1, sponsorNonce + 1);
  const set3 = nftIds.filter((i) => i > 200).slice(0, 750);
  await payoutSet(set3, boomboxRewards3, nonce + 2, sponsorNonce + 2);
})();
