import { AnchorMode, makeContractCall, uintCV } from "@stacks/transactions";
import { handleTransaction, network } from "../src/deploy";
import { boomboxes, poxContractAddress } from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  //"1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);

const pools = [
  { admin: poolAdmin },
  { admin: pool3cycles },
  { admin: pool6cycles },
  { admin: pool700 },
];

(async () => {
  const cycleId = 20;
  for (let p of pools) {
    const admin = p.admin;
    const tx = await makeContractCall({
      contractAddress: poxContractAddress,
      contractName: "pox",
      functionName: "stack-aggregation-commit",
      functionArgs: [rewardPoxAddrCV, uintCV(cycleId)],
      senderKey: admin.private,
      network: network,
      fee: new BN(100000),
      anchorMode: AnchorMode.Any,
    });

    await handleTransaction(tx);
  }
})();
