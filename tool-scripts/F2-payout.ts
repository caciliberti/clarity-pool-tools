import {
  AnchorMode,
  bufferCV,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import {
  FPWR_03_CONTRACT,
  FPWR_03_DEPOT_CONTRACT,
  FPWR_04_DEPOT_CONTRACT,
  stackerReplaceMap,
} from "../src/constants";
import { network, handleTransaction } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  accountsApi,
  downloadPoolPayoutHints,
  getPayoutHints,
  downloadBoomboxesTxs,
  getStackersFromBoombox,
  boomboxes,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const BN = require("bn.js");

const { poolAdmin, poolStxFoundation } = keys;
const dryrun = false;

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce: number,
  memo: string,
  payoutHints: object
) {
  const tx = await makeContractCall({
    contractAddress: FPWR_04_DEPOT_CONTRACT.address,
    contractName: FPWR_04_DEPOT_CONTRACT.name,
    functionName: "add-rewards",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          if (s.stacker in stackerReplaceMap) {
            receiver = (stackerReplaceMap as any)[s.stacker];
            console.log("replaced by mails", s.stacker, receiver);
          }
          if (s.stacker in payoutHints) {
            const stackerHint = (payoutHints as any)[s.stacker];
            if (stackerHint.recipient) {
              receiver = stackerHint.recipient;
              console.log("replaced by hints", s.stacker, receiver);
            }
          }
          return tupleCV({
            user: standardPrincipalCV(receiver),
            amount: uintCV(s.reward),
          });
        })
      ),
    ],
    senderKey: poolAdmin.private,
    nonce: nonce ? new BN(nonce) : undefined,
    network,
    postConditions: [],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const result = await handleTransaction(tx);
    console.log({ result });
  }
}

async function verifyAndPayout(
  stackerSet: {
    rewardCycle: number;
    amount: number;
    stacker: string;
    reward: number;
  }[],
  nonce: number,
  memo: string,
  payoutHints: object
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  console.log({
    stackerSetLength: stackerSet.length,
    totalStackedSet,
    totalPayoutSet,
  });
  await payoutSet(stackerSet, totalPayoutSet, nonce, memo, payoutHints);
}

(async () => {
  const rewardCycle = 14;
  const wrappedRewardsBTC = 186472162;
  const totalRewards = Math.floor((wrappedRewardsBTC * 95) / 100);
  const fees = Math.floor((wrappedRewardsBTC * 5) / 100);

  const memo = `reward cycle #${rewardCycle}`;

  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoolPayoutHints(network);
  const payoutHints = getPayoutHints();

  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    poolStxFoundation.stacks // exclude stx foundation pool
  );
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ countBoomboxes: boomboxStackers.length });

  const stackersInCycle = stackersToCycle(
    v0Stackers.concat(v1Stackers).concat(boomboxStackers),
    rewardCycle
  );

  const boomboxesInCycle = stackersToCycle(boomboxStackers, rewardCycle);

  const stackers = stackersInCycle.members;
  const totalStacked = stackers.reduce((sum: number, s) => sum + s.amount, 0);
  const boomboxStacked = boomboxesInCycle.members.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  const adjustedTotalRewards =
    totalRewards - (boomboxStacked * totalRewards) / totalStacked;

  const filteredExtendedStackers = stackers
    .filter(
      (s) =>
        boomboxesInCycle.members.findIndex((m) => m.stacker === s.stacker) < 0
    )
    .map((stacker) => {
      return {
        reward: Math.round((stacker.amount * totalRewards) / totalStacked),
        ...stacker,
      };
    });
  const totalPayout = filteredExtendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log({
    totalStacked,
    rewards: wrappedRewardsBTC,
    totalRewards,
    fees,
    numberOfStackers: filteredExtendedStackers.length,
    adjustedTotalRewards,
    totalPayout,
  });
  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  const set1 = filteredExtendedStackers.slice(0, 200);
  await verifyAndPayout(set1, nonce, memo, payoutHints);
  const set2 = filteredExtendedStackers.slice(200, 400);
  await verifyAndPayout(set2, nonce + 1, memo, payoutHints);
  const set3 = filteredExtendedStackers.slice(400, 600);
  await verifyAndPayout(set3, nonce + 2, memo, payoutHints);
  const set4 = filteredExtendedStackers.slice(600, 800);
  await verifyAndPayout(set4, nonce + 3, memo, payoutHints);
  const set5 = filteredExtendedStackers.slice(800, 1000);
  await verifyAndPayout(set5, nonce + 4, memo, payoutHints);
  const set6 = filteredExtendedStackers.slice(1000);
  await verifyAndPayout(set6, nonce + 5, memo, payoutHints);
})();
