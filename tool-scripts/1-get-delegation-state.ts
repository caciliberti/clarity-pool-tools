import { readFileSync } from "fs";
import { network } from "../src/deploy";
import {
  downloadPoxTxs,
  writeDelegationStates,
  DelegationDataCV,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const pools = [
  { admin: poolAdmin },
  { admin: pool3cycles },
  { admin: pool6cycles },
  { admin: pool700 },
];

async function writeDelegationAllStates() {
  const fromBlock = 35000;
  const locked = JSON.parse(readFileSync("members-19.json").toString()).members;
  const states: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[] = [];
  const txs = await downloadPoxTxs(network);

  for (let p of pools) {
    await writeDelegationStates(txs, p.admin.stacks, locked, states, fromBlock);
    console.log({ statesSoFar: states.length });
  }
}

(async () => {
  await writeDelegationAllStates();
})();
