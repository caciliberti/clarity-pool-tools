import { AnchorMode, makeSTXTokenTransfer } from "@stacks/transactions";
import BN from "bn.js";
import { handleTransaction, network } from "../src/deploy";
import { keys } from "./config";

const sender = keys.poolAdmin;
const nonce = new BN(1815);
const fee = new BN(1100);
const recipient = keys.pool700.stacks;

(async () => {
  const transaction = await makeSTXTokenTransfer({
    recipient,
    amount: new BN(1000),
    senderKey: sender.private,
    network: network,
    nonce: nonce,
    fee: fee,
    anchorMode: AnchorMode.Any,
  });
  return handleTransaction(transaction);
})();
