import { getPartialStacked } from "../src/pool-tool-partials";
import { boomboxes } from "../src/pool-tool-utils";
import { keys } from "./config";

const {
  poolAdmin,
  pool3cycles,
  pool6cycles,
  poolCcycles,
  pool700,
  poolStxFoundation,
} = keys;
const btcAddr2 = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";
const btcAddrStx = "38dSq9XfVhu51PH51Rpsirr4XgymikdGu1";

(async () => {
  const cycleId = 19;
  let total = 0;
  for (let adminName of [
    ...Object.keys(keys),
    `${boomboxes.address}.${boomboxes.name}`,
  ]) {
    const admin = (keys as any)[adminName] || {
      stacks: `${boomboxes.address}.${boomboxes.name}`,
    };
    const partial = await getPartialStacked(
      admin.stacks,
      cycleId,
      admin.stacks === poolStxFoundation.stacks ? btcAddrStx : btcAddr2
    );
    if (partial) {
      if (partial.amount === "none") {
        console.log(
          partial.cycle,
          partial.poolAdmin,
          partial.btcAddress,
          "none"
        );
      } else {
        const amount = parseInt(partial.amount);
        total += amount;
        console.log(
          partial.cycle,
          partial.poolAdmin,
          partial.btcAddress,
          (amount / 1000000).toLocaleString(undefined, {
            style: "currency",
            currency: "STX",
          })
        );
      }
    }
  }
  console.log("===================================");
  console.log(
    "total:",
    (total / 1000000).toLocaleString(undefined, {
      style: "currency",
      currency: "STX",
    })
  );
})();
