import { updateRewardAdmin } from "../src/fpwr-utils";
import { keys } from "./config";

const { poolAdmin } = keys;
(async () => {
  updateRewardAdmin(poolAdmin, 442);
})();
