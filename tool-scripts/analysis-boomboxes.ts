import { network } from "../src/deploy";
import {
  logBoomboxesContractCSV,
  downloadBoomboxesTxs,
} from "../src/pool-tool-utils";

(async () => {
  await downloadBoomboxesTxs(network);
  await logBoomboxesContractCSV(network);
})();
