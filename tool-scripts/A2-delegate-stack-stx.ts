import { cvToString } from "@stacks/transactions";
import { mainnet } from "../src/deploy";
import {
  accountsApi,
  cycleLength,
  firstBurnChainBlock,
  infoApi,
  stackDelegatedStxsInBatches,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";

const { poolStxFoundation } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "38dSq9XfVhu51PH51Rpsirr4XgymikdGu1"
);

(async () => {
  const length = 10;
  // Change here start
  const indices = undefined;
  const lockingPeriod = 12;
  const admin = poolStxFoundation;
  // change here end
  const info = await infoApi.getCoreApiInfo();
  console.log(info.burn_block_height);
  const cycleId = mainnet ? 12 : 124;
  const startBlock = firstBurnChainBlock + cycleId * cycleLength - 100; // start block of next cycle
  const startBurnHeight = startBlock - 150;

  console.log(cvToString(rewardPoxAddrCV));

  let accountInfo = await accountsApi.getAccountInfo({
    principal: admin.stacks,
    proof: 0,
  });

  await stackDelegatedStxsInBatches(
    indices,
    length,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    startBlock + cycleLength * lockingPeriod,
    cycleId,
    false,
    admin,
    accountInfo.nonce
  );
})();
