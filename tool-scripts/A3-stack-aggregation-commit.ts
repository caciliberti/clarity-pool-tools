import { AnchorMode, makeContractCall, uintCV } from "@stacks/transactions";
import { handleTransaction, network } from "../src/deploy";
import { poxContractAddress } from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";

const { poolStxFoundation } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "38dSq9XfVhu51PH51Rpsirr4XgymikdGu1"
);

(async () => {
  const cycleId = 20; // 18 done 2021-10-03, 19 done 2021-10-03
  const admin = poolStxFoundation;
  const tx = await makeContractCall({
    contractAddress: poxContractAddress,
    contractName: "pox",
    functionName: "stack-aggregation-commit",
    functionArgs: [rewardPoxAddrCV, uintCV(cycleId)],
    senderKey: admin.private,
    network: network,
    fee: new BN(9000),
    anchorMode: AnchorMode.Any,
  });

  console.log(tx);
  await handleTransaction(tx);
})();
