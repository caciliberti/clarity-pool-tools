import {
  AnchorMode,
  bufferCV,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  sponsorTransaction,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { readFileSync } from "fs";
import { stackerReplaceMap } from "../src/constants";
import { network, handleTransaction } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  accountsApi,
  downloadPoolPayoutHints,
  getPayoutHints,
  getStackersFromBoombox,
  boomboxes,
  downloadBoomboxesTxs,
  downloadPoxTxs,
  getStackersFromPox,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const BN = require("bn.js");

const {
  poolAdmin,
  pool3cycles,
  pool6cycles,
  pool700,
  poolStxFoundation,
  fpoo1Payout,
} = keys;

const dryrun = true;
const sendManyFee = 180_000;

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          if (s.stacker in stackerReplaceMap) {
            receiver = (stackerReplaceMap as any)[s.stacker];
            console.log("replaced by mails", s.stacker, receiver);
          }
          if (s.stacker in payoutHints) {
            const stackerHint = (payoutHints as any)[s.stacker];
            if (stackerHint.recipient) {
              receiver = stackerHint.recipient;
              console.log("replaced by hints", s.stacker, receiver);
            }
          }
          return tupleCV({
            to: standardPrincipalCV(receiver),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from(memo)),
          });
        })
      ),
    ],
    senderKey: fpoo1Payout.private,
    nonce: nonce ? new BN(nonce) : undefined,
    fee: new BN(sendManyFee),
    network,
    sponsored: true,
    postConditions: [
      makeStandardSTXPostCondition(
        fpoo1Payout.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(sendManyFee),
      sponsorPrivateKey: poolAdmin.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result });
  }
}

async function logAndPayout(
  stackerSet: {
    rewardCycle: number;
    amount: number;
    stacker: string;
    reward: number;
  }[],
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  console.log({
    stackerSetLength: stackerSet.length,
    totalStackedSet,
    totalPayoutSet,
    nonce,
  });
  await payoutSet(
    stackerSet,
    totalPayoutSet,
    nonce,
    sponsorNonce,
    memo,
    payoutHints
  );
}

(async () => {
  const rewardCycle = 19;
  const rewardsBTC = 1.2152318;
  const exchangeRate = 3160;
  const rewardsBTC2 = 0;
  const exchangeRate2 = 2534;
  const totalRewards =
    (rewardsBTC * 100000000 * 1000000) / exchangeRate +
    (rewardsBTC2 * 100000000 * 1000000) / exchangeRate2;

  const memo = `reward cycle #${rewardCycle} payout`;

  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadPoxTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoolPayoutHints(network);
  const payoutHints = getPayoutHints();

  // get stackers from all cycles
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  console.log({ v0StackersLength: v0Stackers.length });
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    poolStxFoundation.stacks // exclude advocate pool members
  );
  console.log({ v1StackersLength: v1Stackers.length });
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ countBoomboxes: boomboxStackers.length, name: boomboxes.name });

  const poxStackers = getStackersFromPox(network, [
    poolAdmin,
    pool3cycles,
    pool6cycles,
    pool700,
  ]);

  console.log({ countPox: poxStackers.length });

  // filter stackers by cycle
  const stackersInCycle = stackersToCycle(
    v0Stackers.concat(v1Stackers).concat(boomboxStackers).concat(poxStackers),
    rewardCycle
  );

  const boomboxesInCycle = stackersToCycle(boomboxStackers, rewardCycle);
  const poxInCycle = stackersToCycle(poxStackers, rewardCycle);

  const stackers = stackersInCycle.members;
  const totalStacked = stackers.reduce((sum: number, s) => sum + s.amount, 0);
  const boomboxStacked = boomboxesInCycle.members.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );

  const adjustedTotalRewards =
    totalRewards - (boomboxStacked * totalRewards) / totalStacked;
  const filteredExtendedStackers = stackers
    // don't payout boombox stackers now
    .filter(
      (s) =>
        boomboxesInCycle.members.findIndex((m) => m.stacker === s.stacker) < 0
    )
    .map((stacker) => {
      const reward = Math.round((stacker.amount * totalRewards) / totalStacked);
      return {
        reward,
        ...stacker,
      };
    })
    // done try to payout rewards === 0
    .filter((s) => s.reward > 0);

  const totalPayout = filteredExtendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log({
    totalStacked,
    totalRewards,
    numberOfStackers: stackers.length,
    numberOfStackersToPay: filteredExtendedStackers.length,
    adjustedTotalRewards,
    totalPayout,
    numberOfBoomboxers: boomboxesInCycle.members.length,
    boomboxPayout: (boomboxStacked * totalRewards) / totalStacked,
  });

  // get nonce
  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let sponsorNonce = sponsorAccountInfo.nonce;
  console.log({ nonce, sponsorNonce });
  const set1 = filteredExtendedStackers.slice(0, 200);
  await logAndPayout(set1, nonce, sponsorNonce, memo, payoutHints);
  const set2 = filteredExtendedStackers.slice(200, 400);
  await logAndPayout(set2, nonce + 1, sponsorNonce + 1, memo, payoutHints);
  const set3 = filteredExtendedStackers.slice(400, 600);
  await logAndPayout(set3, nonce + 2, sponsorNonce + 2, memo, payoutHints);
  const set4 = filteredExtendedStackers.slice(600, 800);
  await logAndPayout(set4, nonce + 3, sponsorNonce + 3, memo, payoutHints);
  const set5 = filteredExtendedStackers.slice(800, 1000);
  await logAndPayout(set5, nonce + 4, sponsorNonce + 4, memo, payoutHints);
  const set6 = filteredExtendedStackers.slice(1000, 1200);
  await logAndPayout(set6, nonce + 5, sponsorNonce + 5, memo, payoutHints);
  const set7 = filteredExtendedStackers.slice(1200);
  await logAndPayout(set7, nonce + 6, sponsorNonce + 6, memo, payoutHints);
})();
