import { ConsoleLogger } from "@blockstack/clarity-native-bin/lib/logger";
import { writeFileSync } from "fs";
import { network } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  downloadBoomboxesTxs,
  boomboxes,
  getStackersFromBoombox,
  downloadPoxTxs,
  getStackersFromPox,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, pool700 } = keys;
(async () => {
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoxTxs(network);

  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    "SPSTX06BNGJ2CP1F6WA8V49B6MYD784N6YZMK95G" // exclude stx foundation pool
  );
  const poxStackers = getStackersFromPox(network, [
    poolAdmin,
    pool3cycles,
    pool6cycles,
    pool700,
  ]);
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ boomboxCount: boomboxStackers.length });
  const stackers = v0Stackers
    .concat(v1Stackers)
    .concat(poxStackers)
    .concat(boomboxStackers);

  const cycle = 20;
  const stackersInCycle = stackersToCycle(stackers, cycle);
  writeFileSync(`members-${cycle}.json`, JSON.stringify(stackersInCycle));
})();
